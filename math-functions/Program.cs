﻿using System;

namespace math_functions
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                //Главное меню
                Console.WriteLine("Программа для выполнения математических операций:");
                Console.WriteLine("1 - Возведение в степень");
                Console.WriteLine("2 - Извлечение квадратного корня");
                Console.WriteLine("3 - Извлечение кубического корня");
                Console.WriteLine("4 - Логарифм");
                Console.WriteLine("5 - Тетрация (степенная башня)");
                //Считывание нажатой клавиши для определения выбранной операции
                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        {
                            //Возведение в степень
                            Console.Write("\nВведите основание степени: ");
                            int a = Convert.ToInt32(Console.ReadLine());

                            Console.Write("Введите показатель степени: ");
                            int n = Convert.ToInt32(Console.ReadLine());

                            Console.WriteLine($"Число {a} в степени {n} равняется {Math.Pow(a, n)}");
                            break;
                        }
                    case '2':
                        {
                            //Квадратный корень
                            Console.Write("\nВведите число: ");
                            Console.WriteLine("Квадратный корень равен {0}",
                            Math.Round(Math.Sqrt(Convert.ToDouble(Console.ReadLine())), 2));
                            break;
                        }
                    case '3':
                        {
                            //Кубический корень
                            Console.Write("\nВведите число: ");
                            Console.WriteLine("Кубически корень равен {0}",
                            Math.Round(Math.Pow(Convert.ToDouble(Console.ReadLine()), 1 / 3f), 2));
                            break;
                        }
                    case '4':
                        {
                            Console.Write("\nВведите число, логарифм которого нужно получить: ");
                            double b = Convert.ToDouble(Console.ReadLine());
                            Console.Write("Введите основание логарифма: ");
                            double a = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine($"Логарифм {Math.Round(b, 2)} по основанию {Math.Round(a, 2)} равен {Math.Log(b, a)}");
                            break;
                        }
                    case '5':
                        {
                            Console.Write("Введите основание степенной башни: ");
                            double a = Convert.ToDouble(Console.ReadLine());
                            Console.Write($"Значение степенной башни второй степени с основанием {a} равна {Math.Pow(a, a)}");
                            break;
                        }
                }
            }
        }
    }
}
